FROM node:18-bullseye

ARG GIT_COMMIT
ENV GIT_COMMIT=$GIT_COMMIT

ENV NODE_OPTIONS=--use-openssl-ca

# install packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y nano && rm -rf /var/lib/apt/lists/*
